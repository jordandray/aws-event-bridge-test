package com.slothwork.awseventbridgetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwsEventBridgeTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(AwsEventBridgeTestApplication.class, args);
    }

}
