package com.slothwork.awseventbridgetest.model.dto;

import lombok.Data;

@Data
public class ItemDto {
    private int id;
    private String name;
    private String description;
    private int price;
}
