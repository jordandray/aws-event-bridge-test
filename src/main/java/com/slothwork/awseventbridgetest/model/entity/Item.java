package com.slothwork.awseventbridgetest.model.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "item")
public class Item {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name", length = 250, nullable = false)

    private String name;

    @Column(name = "description", length = 250, nullable = false)
    private String description;

    @Column(name = "price", nullable = false)
    private int price;
}
