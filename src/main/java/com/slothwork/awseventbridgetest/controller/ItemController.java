package com.slothwork.awseventbridgetest.controller;

import com.slothwork.awseventbridgetest.model.dto.ItemDto;
import com.slothwork.awseventbridgetest.model.entity.Item;
import com.slothwork.awseventbridgetest.service.ItemService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/item")
public class ItemController {
    private final ItemService itemService;

    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }

    @GetMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<Item> getItemById(@PathVariable("id") int id) {
        ResponseEntity<Item> result = null;
        Item item = itemService.getItemById(id);

        if (item != null) {
            result = new ResponseEntity<>(item, HttpStatus.OK);
        } else {
            result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return result;
    }

    @PutMapping
    public ResponseEntity<Item> createItem(@RequestBody ItemDto itemDto) {
        try {
            return new ResponseEntity<>(itemService.merge(itemDto), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping
    public ResponseEntity<Item> updateItem(@RequestBody ItemDto itemDto) {
        try {
            return new ResponseEntity<>(itemService.merge(itemDto), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<Object> deleteItem(@PathVariable("id") int id) {
        try {
            itemService.getItemById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
