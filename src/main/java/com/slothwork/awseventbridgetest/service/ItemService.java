package com.slothwork.awseventbridgetest.service;

import com.slothwork.awseventbridgetest.model.dto.ItemDto;
import com.slothwork.awseventbridgetest.model.entity.Item;
import com.slothwork.awseventbridgetest.repository.ItemRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.eventbridge.EventBridgeClient;
import software.amazon.awssdk.services.eventbridge.model.PutEventsRequest;
import software.amazon.awssdk.services.eventbridge.model.PutEventsRequestEntry;
import software.amazon.awssdk.services.eventbridge.model.PutEventsResponse;
import software.amazon.awssdk.services.eventbridge.model.PutEventsResultEntry;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ItemService {
    private ItemRepository itemRepository;

    public ItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public Item getItemById(int id) {
        Optional<Item> item = itemRepository.findById(id);
        return item.orElse(null);
    }

    public Item merge(ItemDto itemDto) {
        Item item = new Item();
        BeanUtils.copyProperties(itemDto, item);
        itemRepository.saveAndFlush(item);

        AwsBasicCredentials awsCreds = AwsBasicCredentials.create(
                "****", // Commented out my AWS crdentials
                "****"
        );

        EventBridgeClient eventBridgeClient =
                EventBridgeClient.builder()
                        .region(Region.US_EAST_2)
                        .credentialsProvider(StaticCredentialsProvider.create(awsCreds))
                        .build();

        PutEventsRequestEntry requestEntry = PutEventsRequestEntry.builder()
                .resources("arn:aws:events:us-east-2:342187231054:event-bus/default")
                .source("aws-eb-test")
                .detailType("AWS EB - Test")
                .detail("{ \"key1\": \"value1\", \"key2\": \"value2\" }")
                .build();

        List<PutEventsRequestEntry> list = new ArrayList<PutEventsRequestEntry>();
        list.add(requestEntry);

        PutEventsRequest eventsRequest = PutEventsRequest.builder()
                .entries(requestEntry)
                .build();

        PutEventsResponse result = eventBridgeClient.putEvents(eventsRequest);

        for (PutEventsResultEntry resultEntry : result.entries()) {
            if (resultEntry.eventId() != null) {
                System.out.println("Event Id: " + resultEntry.eventId());
            } else {
                System.out.println("Injection failed with Error Code: " + resultEntry.errorCode());
            }
        }
        return item;
    }

    public void delete(int id) {
        itemRepository.deleteById(id);
    }
}
