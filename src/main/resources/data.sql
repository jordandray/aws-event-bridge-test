DROP TABLE IF EXISTS item;

CREATE TABLE item (
                              id INT AUTO_INCREMENT  PRIMARY KEY,
                              name VARCHAR(250) NOT NULL,
                              description VARCHAR(250) NOT NULL,
                              price INT NOT NULL
);

INSERT INTO item (name, description, price) VALUES
('Pen', 'A standard ink based writing device.', 1),
('#2 Pencils - 12', 'A 12-pack of #2 ye olde fashioned note jotters.', 4),
('Notepad', 'A pad to use all your writing utensils on.', 2);
